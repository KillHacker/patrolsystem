package com.patrolsystem;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

/**
 * Application Controller class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Notify
            final NotificationManager mgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            Notification note = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(Notification.PRIORITY_DEFAULT).build();
            // Hide the notification after its selected
            note.flags |= Notification.FLAG_AUTO_CANCEL;
            note.defaults |= Notification.DEFAULT_ALL;
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;
            mgr.notify(m, note);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
