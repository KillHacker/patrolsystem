package com.patrolsystem.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.patrolsystem.R;
import com.patrolsystem.Utils.CustomRequest;
import com.patrolsystem.Utils.GPSTracker;
import com.patrolsystem.app.AppController;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ScanActivity extends AppCompatActivity implements View.OnClickListener {

    public String location = "";
    public final int SCANNER_ACTIVITY_RESULT = 23;
    public final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 45;
    ProgressDialog progressDialog;
    TextView scanText;
    GPSTracker mGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_activity);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Submitting!");

        Button summaryButton = findViewById(R.id.summary_btn);
        Button scanButton = findViewById(R.id.scan_btn);
        Button submitButton = findViewById(R.id.submit_btn);
        summaryButton.setOnClickListener(this);
        scanButton.setOnClickListener(this);
        submitButton.setOnClickListener(this);

        scanText = findViewById(R.id.scan_text);

        mGPS = new GPSTracker(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.summary_btn:
                startActivity(new Intent(this, ScanListActivity.class));
                break;
            case R.id.scan_btn:
                startActivityForResult(new Intent(this, ScannerActivity.class), SCANNER_ACTIVITY_RESULT);
                break;
            case R.id.submit_btn:
                if (location == null || location.isEmpty())
                    Toast.makeText(this, "Scan a location first!", Toast.LENGTH_LONG).show();
                else
                    submitLocation();
                break;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_item) {
            AppController.getInstance().mUser = null;

            SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
            sharedPreferencesEditor.clear();
            sharedPreferencesEditor.apply();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void submitLocation() {

        mGPS.getLocation();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        } else {
            if (mGPS.canGetLocation()) {
                final String gpsLocation = "lat:" + mGPS.getLatitude() + ", lon:" + mGPS.getLongitude();

                StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "submitLocation",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            progressDialog.hide();
                            try {
                                JSONObject object = new JSONObject(response);

                                if (object.getBoolean("error")) {
                                    String message = object.getString("message");
                                    Log.e("Volley", message);
                                    Toast.makeText(ScanActivity.this, message, Toast.LENGTH_LONG).show();
                                } else {
                                    scanText.setText(R.string.scan_text);
                                    location = "";
                                    Toast.makeText(ScanActivity.this, "Submitted!", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                Log.e("Volley", e.getMessage());
                                Toast.makeText(ScanActivity.this, "Error!", Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.e("Volley", error.toString());
                            progressDialog.hide();
                            Toast.makeText(ScanActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("owner", AppController.getInstance().mUser.id + "");
                        params.put("location", location);
                        params.put("gpsLocation", gpsLocation);

                        return params;
                    }
                };

                AppController.getInstance().addToRequestQueue(postRequest);

                progressDialog.show();
            } else {
                //Enable GPS
                Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(gpsOptionsIntent);
                //Toast.makeText(this, "Wasn't able to find your GPS location. Check application permissions in settings", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "You must allow location permission in settings.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SCANNER_ACTIVITY_RESULT:
                if (data != null && data.hasExtra("result")) {
                    location = data.getStringExtra("result");
                    scanText.setText(location);
                }
                break;
        }
    }
}
