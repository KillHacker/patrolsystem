package com.patrolsystem.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.patrolsystem.Interfaces.OnDoneListener;
import com.patrolsystem.R;
import com.patrolsystem.Utils.CustomRequest;
import com.patrolsystem.adapters.ScansAdapter;
import com.patrolsystem.app.AppController;
import com.patrolsystem.model.Scan;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScanListActivity extends AppCompatActivity {

    List<Scan> scanList;
    SwipeRefreshLayout swipeRefreshLayout;
    ScansAdapter scansAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_list);

        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        scanList = new ArrayList<>();

        refreshItems();

        scansAdapter = new ScansAdapter(this, scanList, new OnDoneListener() {
            @Override
            public void OnDone() {
                refreshItems();
            }
        });
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(scansAdapter);
    }

    public void refreshItems() {
        StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "getScans",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // response
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getBoolean("error")) {
                            String message = object.getString("message");
                            Log.e("Volley", message);
                            Toast.makeText(ScanListActivity.this, message, Toast.LENGTH_LONG).show();
                        } else {
                            scanList.clear();

                            JSONArray scans = object.getJSONArray("scans");

                            for(int i = 0; i < scans.length(); i++) {
                                Scan scan = Scan.parseJSON(scans.getJSONObject(i).toString());
                                scanList.add(scan);
                            }

                            scansAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        Log.e("Volley", e.getMessage());
                        e.printStackTrace();
                        Toast.makeText(ScanListActivity.this, "Error!", Toast.LENGTH_LONG).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    Log.e("Volley", error.toString());
                    Toast.makeText(ScanListActivity.this, "Error!", Toast.LENGTH_LONG).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("id", AppController.getInstance().mUser.id + "");

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(postRequest);

        swipeRefreshLayout.setRefreshing(true);
    }
}
