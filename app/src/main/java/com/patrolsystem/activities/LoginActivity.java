package com.patrolsystem.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.patrolsystem.MyFirebaseInstanceIDService;
import com.patrolsystem.R;
import com.patrolsystem.Utils.CustomRequest;
import com.patrolsystem.app.AppController;
import com.patrolsystem.model.User;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    TextView usernameView, passView;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameView = findViewById(R.id.input_username);
        passView = findViewById(R.id.input_password);
        loginButton = findViewById(R.id.btn_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging in!");

        SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
        if (sharedPreferences.contains("mUser")) {
            LogIntoApp();
        }

        loginButton.setEnabled(true);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogin(v);
            }
        });
    }

    void LogIntoApp() {
        if (AppController.getInstance().mUser == null)
            return;

        final Context context = this;
        //schedule the timer, to alert server that user is online
        /*new Timer().scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (AppController.getInstance().mUser == null)
                    return;

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                RequestUtils.makeRequest(context, "imHere", params);
            }
        }, 2000, AppController.UpdatePeriodMillis);*/


        MyFirebaseInstanceIDService.syncToken();

        startActivity(new Intent(this, ScanActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));

        progressDialog.dismiss();
        finish();
    }

    public void onLogin(final View view) {
        if (usernameView.getText().toString().isEmpty())
            Toast.makeText(this, "Fill your username!", Toast.LENGTH_LONG).show();
        else if (passView.getText().toString().isEmpty())
            Toast.makeText(this, "Fill your password!", Toast.LENGTH_LONG).show();
        else {
            StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressDialog.hide();
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(view.getContext(), message, Toast.LENGTH_LONG).show();
                            } else {
                                AppController.getInstance().mUser = User.parseJSON(object.getJSONObject("user").toString());

                                SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
                                SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                                sharedPreferencesEditor.putString("mUser", object.getJSONObject("user").toString());
                                sharedPreferencesEditor.apply();

                                LogIntoApp();
                            }
                        } catch (Exception e) {
                            Log.e("Volley", e.getMessage());
                            Toast.makeText(view.getContext(), "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        progressDialog.hide();
                        Toast.makeText(view.getContext(), "Error!", Toast.LENGTH_LONG).show();
                    }
                }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("username", usernameView.getText().toString());
                    params.put("password", passView.getText().toString());

                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(postRequest);

            progressDialog.show();
        }
    }
}
