package com.patrolsystem.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.patrolsystem.Utils.Utils;

/**
 * Created by Mersad on 3/16/2018.
 */

public class Scan {
    public int id;
    public String location, GPSLocation, owner, dateTime;
    public boolean isMine;

    public static Scan parseJSON(String response) {
        Gson gson = new GsonBuilder().registerTypeAdapter(boolean.class, Utils.booleanAsIntAdapter).create();
        return gson.fromJson(response, Scan.class);
    }
}
