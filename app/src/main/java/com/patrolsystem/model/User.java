package com.patrolsystem.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * User model
 *
 * @author Enver
 * @date 15.03.2018.
 */
public class User {

    public String id;
    public String employeeName;
    public int managerId;

    public static User parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(response, User.class);
    }
}
