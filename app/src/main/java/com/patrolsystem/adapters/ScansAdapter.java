package com.patrolsystem.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.patrolsystem.Interfaces.OnDoneListener;
import com.patrolsystem.R;
import com.patrolsystem.Utils.CustomRequest;
import com.patrolsystem.app.AppController;
import com.patrolsystem.model.Scan;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mersad on 3/16/2018.
 */

public class ScansAdapter extends RecyclerView.Adapter<ScansAdapter.MyViewHolder> {
    private List<Scan> scanList;
    private Context context;
    OnDoneListener refreshItemsListener;

    public ScansAdapter(Context context, List<Scan> mScanList, OnDoneListener refreshItemsListener) {
        this.scanList = mScanList;
        this.context = context;
        this.refreshItemsListener = refreshItemsListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.scan_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Scan scan = scanList.get(position);
        holder.input_location.setText(scan.location);
        holder.input_time.setText(scan.dateTime);
        holder.input_owner.setText(scan.owner);
        holder.deleteCard.setVisibility(scan.isMine ? View.VISIBLE : View.INVISIBLE);

        holder.deleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteScan(scan.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return scanList.size();
    }

    public void deleteScan(final int id) {
        StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "deleteScan",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    // response
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getBoolean("error")) {
                            String message = object.getString("message");
                            Log.e("Volley", message);
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                            if (refreshItemsListener != null)
                                refreshItemsListener.OnDone();
                        }
                    } catch (Exception e) {
                        Log.e("Volley", e.getMessage());
                        e.printStackTrace();
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    Log.e("Volley", error.toString());
                    Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                }
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("id", id + "");

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(postRequest);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView deleteCard;
        public TextView input_location, input_time, input_owner;
        public View itemView;

        public MyViewHolder(View view) {
            super(view);
            itemView = view.findViewById(R.id.card_view);
            input_location = view.findViewById(R.id.input_location);
            input_time = view.findViewById(R.id.input_time);
            input_owner = view.findViewById(R.id.input_owner);
            deleteCard = view.findViewById(R.id.image_view_delete);
        }
    }
}
