package com.patrolsystem;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.patrolsystem.Utils.CustomRequest;
import com.patrolsystem.app.AppController;

import java.util.HashMap;
import java.util.Map;

/**
 * Application Controller class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        syncToken();
    }

    public static void syncToken(){
        // Get updated InstanceID token.
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        if (AppController.getInstance().mUser == null)
            return;

        StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "updateToken",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    Log.e("Volley", error.toString());
                    error.printStackTrace();
                }
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("userId", AppController.getInstance().mUser.id + "");
                params.put("token", refreshedToken);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(postRequest);
    }
}
