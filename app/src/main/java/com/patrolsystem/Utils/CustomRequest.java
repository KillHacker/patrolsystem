package com.patrolsystem.Utils;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;

public class CustomRequest extends Request {
    public CustomRequest(int method, String url, Response.ErrorListener listener) {
        super(method, url, listener);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        System.out.println(response.statusCode);
        try {
            Log.d("custom response", "[raw json]: " + (new String(response.data)));
            Gson gson = new Gson();
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(response.data,
                    HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Object arg0) {
        // TODO Auto-generated method stub
    }
}