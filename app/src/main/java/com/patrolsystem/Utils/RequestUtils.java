package com.patrolsystem.Utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.patrolsystem.Interfaces.OnDoneListener;
import com.patrolsystem.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class RequestUtils {

    public static void makeRequest(final Context context, String path, final Map<String, String> params) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Volley", e.toString());
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    public static void makeRequest(final Context context, String path, final Map<String, String> params, final OnDoneListener onDoneListener) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //response
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                            } else
                                onDoneListener.OnDone();

                        } catch (JSONException e) {
                            Log.e("Volley", e.toString());
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(getRequest);
    }
}
